/*
 * Copyright (c) 2015, Jay Elliott
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "battleship.h"

enum AI_EASY_STATE
{
    AI_EASY_SEARCH,
    AI_EASY_CHECK_PENDING,
    AI_EASY_KILL
};

static void ai_place_ships(int player_idx);
static void ai_easy_turn(int player_idx);

struct player_controls ai_easy_controls = {
    .place_ships = ai_place_ships,
    .take_turn = ai_easy_turn
};

static void try_place(int player_idx, char const *ship_name, int ship_size)
{
    int dir;
    int col, row;

    do
    {
        if (rand() % 2)
            dir = DIR_HOR;
        else
            dir = DIR_VERT;

        if (dir == DIR_HOR)
        {
            row = rand() % GRID_HEIGHT;
            col = rand() % (GRID_WIDTH - ship_size);
        }
        else
        {
            row = rand() % (GRID_HEIGHT - ship_size);
            col = rand() % GRID_WIDTH;
        }
    } while (ship_put(players + player_idx, ship_name, row,
                      col, ship_size, dir) != 0);
}

void ai_place_ships(int player_idx)
{
    struct player *p = players + player_idx;

    try_place(player_idx, CARRIER_NAME, CARRIER_SIZE);
    try_place(player_idx, BATTLESHIP_NAME, BATTLESHIP_SIZE);
    try_place(player_idx, CRUISER_NAME, CRUISER_SIZE);
    try_place(player_idx, SUBMARINE_NAME, SUBMARINE_SIZE);
    try_place(player_idx, DESTROYER_NAME, DESTROYER_SIZE);
}

static int ai_pending_pop(struct player *p, int *row, int *col)
{
    if (p->ais.n_pending)
    {
        *row = p->ais.pending_rows[p->ais.n_pending - 1];
        *col = p->ais.pending_cols[p->ais.n_pending - 1];
        p->ais.n_pending--;
        if (!p->ais.n_pending)
        {
            free(p->ais.pending_rows);
            free(p->ais.pending_cols);
            p->ais.pending_rows = p->ais.pending_cols = NULL;
        }
        return 1;
    }
    return 0;
}

static void ai_pending_push(struct player *p, int row, int col)
{
    p->ais.n_pending++;
    p->ais.pending_rows = (int*)realloc(p->ais.pending_rows, p->ais.n_pending * sizeof(int));
    p->ais.pending_cols = (int*)realloc(p->ais.pending_cols, p->ais.n_pending * sizeof(int));
    p->ais.pending_rows[p->ais.n_pending - 1] = row;
    p->ais.pending_cols[p->ais.n_pending - 1] = col;
}


/*
 * state machine:
 *     transition from AI_EASY_SEARCH to AI_EASY_KILL when it hits something.
 *     transition from AI_EASY_KILL to AI_EASY_CHECK_PENDING when it blows up
 *         an entire row or column of pixels (enclosed by grid bounds and/or
 *         missed shots).  If it does not get a kill, then it will fill in the
 *         pending list before the transition occurs.
 *     transition from AI_EASY_CHECK_PENDING to AI_EASY_SEARCH when the pending
 *         list is empty
 *     transition from AI_EASY_CHECK_PENDING to AI_EASY_KILL and dequeue the
 *         pending list whenever in AI_EASY_CHECK_PENDING and the pending list
 *         is not empty.
 *
 * this function always recurses on state changes if it has not fired a shot.
 */
void ai_easy_turn(int player_idx)
{
    struct player *p = players + player_idx;
    int row, col;
    int neg_node, pos_node;
    int fire_result;

    /* up to four neighboring pegs, consisting of row, col, and dir */
    int neigh_count, neighbors[4][3], which_neigh;

    if (p->ais.state == AI_EASY_SEARCH)
    {
        /* pick a random tile that we haven't already fired upon */
        do
        {
            row = rand() % GRID_HEIGHT;
            col = rand() % GRID_WIDTH;
        } while (grid_get(p->shots, row, col) & (FLAG_HIT | FLAG_MISS));

        if (fire(player_idx, row, col) == FIRE_HIT)
        {
            /* transition state. */
            p->ais.state = AI_EASY_KILL;
            p->ais.row = p->ais.row_start = row;
            p->ais.col = p->ais.col_start = col;
            p->ais.dir = DIR_UNKNOWN;
        }
    }
    else if (p->ais.state == AI_EASY_CHECK_PENDING)
    {
        if (ai_pending_pop(p, &p->ais.row_start, &p->ais.col_start))
        {
            p->ais.row = p->ais.row_start;
            p->ais.col = p->ais.col_start;
            p->ais.state = AI_EASY_KILL;
            ai_easy_turn(player_idx);
        }
        else
        {
            p->ais.state = AI_EASY_SEARCH;
            ai_easy_turn(player_idx);
        }
        return;
    }
    else if (p->ais.state == AI_EASY_KILL)
    {
        row = p->ais.row;
        col = p->ais.col;
        if (p->ais.dir == DIR_HOR)
        {
            neg_node = pos_node = p->ais.col;
            /* 
             * find the first un-shot peg on either side of (col, row),
             * If there are two found, then pick one at random.
             */
            do
            {
                neg_node--;
            } while (neg_node >= 0 &&
                     NODE_HIT(grid_get(p->shots, row, neg_node)));
            do
            {
                pos_node++;
            } while (pos_node < GRID_WIDTH &&
                     NODE_HIT(grid_get(p->shots, row, pos_node)));
            if (neg_node >= 0 && NODE_MISS(grid_get(p->shots, row, neg_node)))
                neg_node = -1;
            if (pos_node >= GRID_WIDTH ||
                NODE_MISS(grid_get(p->shots, row, pos_node)))
                pos_node = -1;

            if (neg_node >= 0 && pos_node >= 0)
            {
                /* pick a direction at random */
                if (rand() % 2 == 0)
                    col = neg_node;
                else
                    col = pos_node;
            }
            else if (neg_node >= 0)
            {
                /* pick the left direction */
                col = neg_node;
                
            }
            else if (pos_node >= 0)
            {
                /* pick the right direction */
                col = pos_node;
            }
            else
            {
                /* 
                 * The AI thought there was one horizontal ship, but there were
                 * actually several vertical ships next to each other.
                 */

                /*
                 * This looks like the code above where we got the first nodes which do
                 * not have the hit flag set, but it's a little different because now we
                 * are finding the last nodes which do have the hit flag set.
                 */
                neg_node = pos_node = col;
                do
                {
                    neg_node--;
                } while (neg_node > 0 &&
                         NODE_HIT(grid_get(p->shots, row, neg_node)));
                do
                {
                    pos_node++;
                } while (pos_node < GRID_WIDTH &&
                         NODE_HIT(grid_get(p->shots, row, pos_node)));
                if (neg_node < 0 || !NODE_HIT(grid_get(p->shots, row, neg_node)))
                    neg_node++;
                if (pos_node >= GRID_WIDTH || !NODE_HIT(grid_get(p->shots, row, pos_node)))
                    pos_node--;

                /* add all the rows to ais.penidng */
                p->ais.n_pending = pos_node - neg_node + 1;
                for (col = neg_node; col <= pos_node; col++)
                {
                    ai_pending_push(p, row, col);
                }

                p->ais.dir = DIR_UNKNOWN;
                p->ais.state = AI_EASY_CHECK_PENDING;
                ai_easy_turn(player_idx);
                return;
            }

            if (fire(player_idx, row, col) == FIRE_SINK)
                p->ais.state = AI_EASY_CHECK_PENDING;
        }
        else if (p->ais.dir == DIR_VERT)
        {
            neg_node = pos_node = p->ais.row;
            /* 
             * find the first un-shot peg on either side of (col, row),
             * If there are two found, then pick one at random.
             */
            do
            {
                neg_node--;
            } while (neg_node >= 0 &&
                     NODE_HIT(grid_get(p->shots, neg_node, col)));
            do
            {
                pos_node++;
            } while (pos_node < GRID_WIDTH &&
                     NODE_HIT(grid_get(p->shots, pos_node, col)));
            if (neg_node >= 0 && NODE_MISS(grid_get(p->shots, neg_node, col)))
                neg_node = -1;
            if (pos_node >= GRID_WIDTH || NODE_MISS(grid_get(p->shots, pos_node, col)))
                pos_node = -1;

            if (neg_node >= 0 && pos_node >= 0)
            {
                /* pick a direction at random */
                if (rand() % 2 == 0)
                    row = neg_node;
                else
                    row = pos_node;
            }
            else if (neg_node >= 0)
            {
                /* pick the left direction */
                row = neg_node;
            }
            else if (pos_node >= 0)
            {
                /* pick the right direction */
                row = pos_node;
            }
            else
            {
                /* 
                 * The AI thought there was one vertical ship, but there were
                 * actually several horizontal ships next to each other.
                 */

                /*
                 * This looks like the code above where we got the first nodes which do
                 * not have the hit flag set, but it's a little different because now we
                 * are finding the last nodes which do have the hit flag set.
                 */
                neg_node = pos_node = row;
                do
                {
                    neg_node--;
                } while (neg_node > 0 &&
                         NODE_HIT(grid_get(p->shots, neg_node, col)));
                do
                {
                    pos_node++;
                } while (pos_node < GRID_WIDTH &&
                         NODE_HIT(grid_get(p->shots, pos_node, col)));
                if (neg_node < 0 || !NODE_HIT(grid_get(p->shots, neg_node, col)))
                    neg_node++;
                if (pos_node >= GRID_HEIGHT || !NODE_HIT(grid_get(p->shots, pos_node, col)))
                    pos_node--;

                /* add all the rows to ais.penidng */
                p->ais.n_pending = pos_node - neg_node + 1;
                for (row = neg_node; row <= pos_node; row++)
                {
                    ai_pending_push(p, row, col);
                }

                p->ais.dir = DIR_UNKNOWN;
                p->ais.state = AI_EASY_CHECK_PENDING;
                ai_easy_turn(player_idx);
                return;
            }

            if (fire(player_idx, row, col) == FIRE_SINK)
                p->ais.state = AI_EASY_CHECK_PENDING;
        }
        else /* DIR_UNKNOWN */
        {
            /* pick a random neighbor to shoot at */
            neigh_count = 0;
            if ((row - 1) >= 0 &&
                !(grid_get(p->shots, row - 1, col) & (FLAG_HIT | FLAG_MISS)))
            {
                neighbors[neigh_count][0] = row - 1;
                neighbors[neigh_count][1] = col;
                neighbors[neigh_count][2] = DIR_VERT;
                neigh_count++;
            }
            if ((row + 1) < GRID_HEIGHT &&
                !(grid_get(p->shots, row + 1, col) & (FLAG_HIT | FLAG_MISS)))
            {
                neighbors[neigh_count][0] = row + 1;
                neighbors[neigh_count][1] = col;
                neighbors[neigh_count][2] = DIR_VERT;
                neigh_count++;
            }
            if ((col - 1) >= 0 &&
                !(grid_get(p->shots, row, col - 1) & (FLAG_HIT | FLAG_MISS)))
            {
                neighbors[neigh_count][0] = row;
                neighbors[neigh_count][1] = col - 1;
                neighbors[neigh_count][2] = DIR_HOR;
                neigh_count++;
            }
            if ((col + 1) < GRID_WIDTH &&
                !(grid_get(p->shots, row, col + 1) & (FLAG_HIT | FLAG_MISS)))
            {
                neighbors[neigh_count][0] = row;
                neighbors[neigh_count][1] = col + 1;
                neighbors[neigh_count][2] = DIR_HOR;
                neigh_count++;
            }

            if (neigh_count == 0)
            {
                /* This can happen when we're going down the pending list and
                 * we get to a node which wasn't surrounded by un-shot-at nodes
                 * when it was placed on the list but is now.
                 */
                p->ais.state = AI_EASY_CHECK_PENDING;
                ai_easy_turn(player_idx);
                return;
            }

            which_neigh = rand() % neigh_count;
            fire_result = fire(player_idx, neighbors[which_neigh][0],
                               neighbors[which_neigh][1]);
            if (fire_result == FIRE_SINK)
            {
                p->ais.state = AI_EASY_CHECK_PENDING;
            }
            else if (fire_result == FIRE_HIT)
                p->ais.dir = neighbors[which_neigh][2];
        }
    }
}
