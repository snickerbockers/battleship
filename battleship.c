/*
 * Copyright (c) 2015, Jay Elliott
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <err.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>

#include "battleship.h"

#define INPUT_LEN 128

static char input_buffer[INPUT_LEN];

static void grid_set(int *grid, unsigned val, int row, int col);
static int prompt_dir(void);
static void human_place_ships(int player_idx);
static void human_turn(int player_idx);

struct player players[1 + PLAYER_2];

struct player_controls human_controls = {
    .place_ships = human_place_ships,
    .take_turn = human_turn
};

void player_place_ships(int player_idx)
{
    assert(player_idx <= PLAYER_2);
    players[player_idx].controls->place_ships(player_idx);
}

void player_take_turn(int player_idx)
{
    assert(player_idx <= PLAYER_2);
    players[player_idx].controls->take_turn(player_idx);
}

void player_set_name(int player_idx, char const *name)
{
    assert(player_idx <= PLAYER_2);
    strncpy(players[player_idx].name, name, PLAYER_NAME_LEN);
    players[player_idx].name[PLAYER_NAME_LEN - 1] = '\0';
}

char const *player_get_name(int player_idx)
{
    return players[player_idx].name;
}

/* wrapper for fgets that calls exit(0) if it receives an EOF */
static int read_line(FILE *stream, char *buf, size_t buf_len)
{
    char *c;

    memset(buf, 0, buf_len);
    c = fgets(buf, buf_len, stream);

    if (!c && errno) /* error */
        err(1, "unable to read input");
    else if (!c) /* EOF */
        exit(0);

    return 0;
}

/* (col, row) refers to the left node or top node of the ship */
int ship_put(struct player *player, char const *ship_name, int row, int col,
             int ship_len, int dir)
{
    int node;
    int offset;
    struct ship *meta;

    if (dir == DIR_HOR)
    {
        if (col + ship_len > GRID_WIDTH)
        {
            fprintf(stdout, "Error: that would extend beyond the grid's "
                    "width\n");
            return -1;
        }

        for (offset = 0; offset < ship_len; offset++)
            if (NODE_SHIP(grid_get(player->ships, row, col + offset)))
            {
                fprintf(stdout, "Error: The node at (%d, %d) is already "
                        "occupied by an existing ship\n", col + offset, row);
                return -1;
            }

        for (offset = 0; offset < ship_len; offset++)
        {
            node = grid_get(player->ships, row, col + offset);
            node |= FLAG_SHIP;
            grid_set(player->ships, node, row, col + offset);
        }
    }
    else if (dir == DIR_VERT)
    {
        if (row + ship_len > GRID_HEIGHT)
        {
            fprintf(stdout, "Error: that would extend beyond the grid's "
                    "height\n");
            return -1;
        }

        for (offset = 0; offset < ship_len; offset++)
            if (NODE_SHIP(grid_get(player->ships, row + offset, col)))
            {
                fprintf(stdout, "Error: The node at (%d, %d) is already "
                        "occupied by an existing ship\n", col, row + offset);
                return -1;
            }

        for (offset = 0; offset < ship_len; offset++)
        {
            node = grid_get(player->ships, row + offset, col);
            node |= FLAG_SHIP;
            grid_set(player->ships, node, row + offset, col);
        }
    }

    assert(player->ship_count < N_SHIPS);
    meta = player->ship_meta + player->ship_count++;
    strncpy(meta->name, ship_name, SHIP_NAME_LEN);
    meta->name[SHIP_NAME_LEN - 1] = '\0';
    meta->pos_x = col;
    meta->pos_y = row;
    meta->len = ship_len;
    meta->dir = dir;

    return 0;
}

int grid_get(int const *grid, int row, int col)
{
    unsigned shift;

    assert(row >= 0 && row <= 10);
    assert(col >= 0 && col <= 10);

    shift = 3 * col;
    return (grid[row] & (7 << shift)) >> shift;
}

static void grid_set(int *grid, unsigned val, int row, int col)
{
    unsigned mask;
    unsigned shift;

    assert(!(val & ~7));
    assert(row >= 0 && row < 10);
    assert(col >= 0 && col < 10);

    shift = 3 * col;
    mask = 7 << shift;

    grid[row] = (grid[row] & ~mask) | (val << shift);
}

char const *col_name(int col)
{
    static char *tbl[GRID_WIDTH] = {
        "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"
    };

    if (col < 0 || col >= GRID_WIDTH)
        return NULL;
    return tbl[col];
}

char const *row_name(int row)
{
    static char const *tbl[GRID_HEIGHT] = {
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"
    };

    if (row < 0 || row >= GRID_HEIGHT)
        return NULL;
    return tbl[row];
}

void print_grid(FILE *stream, int *grid, char const *lbl_color, char const *empty_color, char empty,
                char const *hit_color, char hit, char const *ship_color, char ship)
{
    int fildes;
    char ch;
    char const *color;
    int row, col;
    int use_color;
    int node;

    if ((fildes = fileno(stream)) < 0)
    {
        /*
         * XXX may want to perform this check once at startup so we
         * dont flood stderr
         */
        warn("Unable to determine if output stream is a tty; colors "
             "disabled\n");
        use_color = 0;
    }
    else
    {
        if (isatty(fildes))
            use_color = 1;
        else
            use_color = 0;
    }

    if (use_color)
        fprintf(stream, "%s  ", lbl_color);
    else
        fprintf(stream, "   ");

    for (col = 0; col < 10; col++)
        fprintf(stream, "%s ", col_name(col));
    fprintf(stream, "\n");

    for (row = 0; row < 10; row++)
    {
        if (use_color)
            fprintf(stream, "%s%s", lbl_color, row_name(row));
        else
            fprintf(stream, "%s", row_name(row));

        for (col = 0; col < 10; col++)
        {
            node = grid_get(grid, row, col);
            if (NODE_HIT(node))
            {
                color = hit_color;
                ch = hit;
            }
            else if (NODE_SHIP(node))
            {
                color = ship_color;
                ch = ship;
            }
            else if (NODE_MISS(node))
            {
                color = empty_color;
                ch = empty;
            }
            else
            {
                color = COLOR_DEF;
                ch = ' ';
            }

            if (use_color)
                fprintf(stream, " %s%c", color, ch);
            else
                fprintf(stream, " %c", ch);
        }
        fprintf(stream, "\n");
    }

    /* reset the color to default */
    if (use_color)
        fprintf(stream, "%s", COLOR_DEF);
}

void print_player_ships(int player_idx)
{
    struct player *p = players + player_idx;

    printf("Your ships:\n");
    print_grid(stdout, p->ships, COLOR_DEF, COLOR_DEF, ' ', COLOR_RED, 'X', COLOR_BLUE, 'B');
}

void print_player_targetting(int player_idx)
{
    struct player *p = players + player_idx;
    printf("Your targetting board:\n");
    print_grid(stdout, p->shots, COLOR_DEF, COLOR_WHITE, 'O', COLOR_RED, 'X', COLOR_DEF, ' ');
}

void new_game(void)
{
    memset(players, 0, sizeof(struct player) * 2);
    srand(time(NULL));
}

static int prompt_dir(void)
{
    for (;;)
    {
        printf("place ship (h)orizontal or (v)ertically?: ");

        read_line(stdin, input_buffer, sizeof(input_buffer));
        if (strcmp(input_buffer, "v\n") == 0)
            return DIR_VERT;
        else if (strcmp(input_buffer, "h\n") == 0)
            return DIR_HOR;
        else
            printf("press \'v\' for vertical or \'h\' for horizontally.  Then press enter.\n");
    }
}

/*
 * This function will accept any format which has one letter and one number,
 * so A-1, A.1 (1, A) A1, 1A, and many more are all valid.
 */
static void prompt_peg(int *rowp, int *colp)
{
    char const *next;
    int row, col;

    for (;;)
    {
        row = col = -1;
        printf("Enter position: ");
        read_line(stdin, input_buffer, sizeof(input_buffer));
        next = input_buffer;
        while (*next)
        {
            if (*next >= 'A' && *next < 'A' + GRID_HEIGHT)
            {
                if (row >= 0)
                {
                    printf("Ambiguous expression; there are more than one rows "
                           "specified\n");
                    break;
                }
                row = *next - 'A';
            }
            if (*next >= 'a' && *next < 'a' + GRID_HEIGHT)
            {
                if (row >= 0)
                {
                    printf("Ambiguous expression; there are more than one rows "
                           "specified\n");
                    break;
                }
                row = *next - 'a';
            }
            if (*next >= '1' && *next < '0' + GRID_WIDTH)
            {
                if (col >= 0)
                {
                    printf("Ambiguous expression; there are more than one "
                           "columns specified\n");
                    break;
                }
                if (*next == '1' && next[1] == '0')
                {
                    /* special case: 10 is two digits instead of one */
                    next++;
                    col = 9;
                }
                else
                    col = *next - '1';
            }
            next++;
        }
        if (row >= 0 && col >= 0)
        {
            *rowp = row;
            *colp = col;
            return;
        }
    }
}

static void human_place_ships(int player_idx)
{
    char line;
    struct player *p = players + player_idx;
    int row, col, dir;

    do
    {
        fputc('\n', stdout);
        print_player_ships(player_idx);
        printf("Place your %s (%d pegs)\n", DESTROYER_NAME, DESTROYER_SIZE);
        dir = prompt_dir();
        prompt_peg(&row, &col);
    } while (ship_put(p, DESTROYER_NAME, row, col, 2, dir) != 0);

    do
    {
        fputc('\n', stdout);
        print_player_ships(player_idx);
        printf("Place your %s (%d pegs)\n", SUBMARINE_NAME, SUBMARINE_SIZE);
        dir = prompt_dir();
        prompt_peg(&row, &col);
    } while (ship_put(p, SUBMARINE_NAME, row, col, 3, dir) != 0);

    do
    {
        fputc('\n', stdout);
        print_player_ships(player_idx);
        printf("Place your %s (%d pegs)\n", CRUISER_NAME, CRUISER_SIZE);
        dir = prompt_dir();
        prompt_peg(&row, &col);
    } while (ship_put(p, CRUISER_NAME, row, col, 3, dir) != 0);

    do
    {
        fputc('\n', stdout);
        print_player_ships(player_idx);
        printf("Place your %s (%d pegs)\n", BATTLESHIP_NAME, BATTLESHIP_SIZE);
        dir = prompt_dir();
        prompt_peg(&row, &col);
    } while (ship_put(p, BATTLESHIP_NAME, row, col, 4, dir) != 0);

    do
    {
        fputc('\n', stdout);
        print_player_ships(player_idx);
        printf("Place your %s (%d pegs)\n", CARRIER_NAME, CARRIER_SIZE);
        dir = prompt_dir();
        prompt_peg(&row, &col);
    } while (ship_put(p, CARRIER_NAME, row, col, 5, dir) != 0);
}

int fire(int player_idx, int row, int col)
{
    struct player *p_src, *p_tgt;
    struct ship *ship_meta, *ship_sank;
    int node_tgt, node_shot, ret_val;
    int ship_idx, node_offset, node_ship;

    assert(row < GRID_HEIGHT);
    assert(col < GRID_WIDTH);

    if (player_idx == PLAYER_1)
    {
        p_src = players + PLAYER_1;
        p_tgt = players + PLAYER_2;
    }
    else
    {
        p_src = players + PLAYER_2;
        p_tgt = players + PLAYER_1;
    }

    printf("%s fires on (%s, %s)\n", p_src->name, col_name(col), row_name(row));

    ret_val = 0;
    node_tgt = grid_get(p_tgt->ships, row, col);
    node_shot = grid_get(p_src->shots, row, col);
    if (NODE_SHIP(node_tgt))
    {
        assert(!(NODE_HIT(node_tgt) || NODE_MISS(node_tgt)));

        ret_val = FIRE_HIT;
        node_tgt |= FLAG_HIT;
        node_shot = node_tgt;
    }
    else
    {
        ret_val = FIRE_MISS;
        node_shot |= FLAG_MISS;
    }
    grid_set(p_tgt->ships, node_tgt, row, col);
    grid_set(p_src->shots, node_shot, row, col);

    /* detect if a ship just sank */
    if (ret_val)
    {
        ship_sank = NULL;
        for (ship_idx = 0; ship_idx < p_tgt->ship_count; ship_idx++)
        {
            ship_meta = p_tgt->ship_meta + ship_idx;
            if (ship_meta->dir == DIR_HOR && row == ship_meta->pos_y)
            {
                if (col >= ship_meta->pos_x &&
                    col < ship_meta->pos_x + ship_meta->len)
                {
                    for (node_offset = 0; node_offset < ship_meta->len;
                         node_offset++)
                    {
                        node_ship = grid_get(p_tgt->ships, row,
                                             ship_meta->pos_x + node_offset);
                        if (!NODE_HIT(node_ship))
                            goto on_exit;
                    }
                    ship_sank = ship_meta;
                    break;
                }
            }
            else if (ship_meta->dir == DIR_VERT && col == ship_meta->pos_x)
            {
                if (row >= ship_meta->pos_y &&
                    row < ship_meta->pos_y + ship_meta->len)
                {
                    for (node_offset = 0; node_offset < ship_meta->len;
                         node_offset++)
                    {
                        node_ship = grid_get(p_tgt->ships,
                                             ship_meta->pos_y + node_offset,
                                             col);
                        if (!NODE_HIT(node_ship))
                            goto on_exit;
                    }
                    ship_sank = ship_meta;
                    break;
                }
            }
        }

        if (ship_sank)
        {
            ret_val = FIRE_SINK;
            printf("%s sank %s\'s %s\n", p_src->name, p_tgt->name,
                   ship_sank->name);
        }
    }

on_exit:
    return ret_val;
}

int game_over(int *winner)
{
    struct player *p;
    int player, row, col, node, not_a_loser;

    for (player = 0; player < 2; player++)
    {
        not_a_loser = 0;
        p = players + player;
        for (row = 0; row < GRID_HEIGHT; row++)
        {
            for (col = 0; col < GRID_WIDTH; col++)
            {
                node = grid_get(p->ships, row, col);
                if (NODE_SHIP(node) && !NODE_HIT(node))
                {
                    not_a_loser = 1;
                    break;
                }
            }
            if (not_a_loser)
                break;
        }

        if (!not_a_loser)
        {
            *winner = !player;
            return 1;
        }
    }

    return 0;
}

static void human_turn(int player_idx)
{
    int row, col, node;

    for (;;)
    {
        prompt_peg(&row, &col);
        node = grid_get(players[player_idx].shots, row, col);

        if (!NODE_HIT(node) && !NODE_MISS(node))
            break;
        else
            printf("You've already targeted that node.\n");
    }

    if (fire(player_idx, row, col))
        printf("Hit!\n");
    else
        printf("Miss\n");
}

void game_loop(void)
{
    int winner;

    for (;;)
    {
        print_player_targetting(PLAYER_1);
        printf("\n");
        print_player_ships(PLAYER_1);

        player_take_turn(PLAYER_1);
        if (game_over(&winner))
            break;
        player_take_turn(PLAYER_2);
        if (game_over(&winner))
            break;
    }

    printf("The winner is %s\n", player_get_name(winner));
}

void main_menu(void)
{
    printf("Welcome to Battleship\n");
    printf("Copyright 2015, Jay Elliott\n");
    printf("This software is licensed under a BSD-style license; See the "
           "included COPYING file for details.\n");
    printf("\n");
    printf("\n");
    printf("\n");
    printf("This game is still a work-in-progress.  Beginning a 1v1 game against the A.I...\n");

    new_game();
    player_set_name(PLAYER_1, "Player 1");
    player_set_name(PLAYER_2, "A.I. (easy)");
    players[PLAYER_1].controls = &human_controls;
    players[PLAYER_2].controls = &ai_easy_controls;

    player_place_ships(PLAYER_1);
    player_place_ships(PLAYER_2);

    game_loop();
}

int main(int argc, char **argv)
{
    main_menu();

    return 0;
}
