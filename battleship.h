/*
 * Copyright (c) 2015, Jay Elliott
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* VT100 escape sequences */
#define COLOR_RED   "\x1b[31m"
#define COLOR_BLUE  "\x1b[34m"
#define COLOR_WHITE "\x1b[37m"
#define COLOR_DEF   "\x1b[39m"

/*
 * PLAYER_1 is always a humman.
 * PLAYER_2 is always an AI until I implement multiplayer
 */
#define PLAYER_1 0
#define PLAYER_2 1
#define PLAYER_NAME_LEN 16

#define SHIP_NAME_LEN 16
#define N_SHIPS 5

#define GRID_WIDTH 10
#define GRID_HEIGHT 10

#define DESTROYER_SIZE  2
#define SUBMARINE_SIZE  3
#define CRUISER_SIZE    3
#define BATTLESHIP_SIZE 4
#define CARRIER_SIZE    5

#define DESTROYER_NAME "destroyer"
#define SUBMARINE_NAME "submarine"
#define CRUISER_NAME "cruiser"
#define BATTLESHIP_NAME "battleship"
#define CARRIER_NAME "carrier"

/*
 * flags for the grid bitfields.  if neither hit nor miss is set, then that
 * means that this node has not been fired upon.
 */
#define FLAG_HIT 0x1
#define FLAG_MISS 0x2
#define FLAG_SHIP 0x4

#define NODE_HIT(n) ((n) & FLAG_HIT)
#define NODE_MISS(n) ((n) & FLAG_MISS)
#define NODE_SHIP(n) ((n) & FLAG_SHIP)

/* ship orientations.  DIR_UNKNOWN is only used by the AI */
#define DIR_UNKNOWN 0
#define DIR_HOR  1
#define DIR_VERT 2

/*
 * return codes for the fire function.
 */
enum
{
    FIRE_MISS = 0,
    FIRE_HIT,
    FIRE_SINK
};

/*
 * ship data.  This is only used to print those "you sunk my XXX messages";
 * the actual game logic is implemented solely using the ships grid in the
 * player struct.
 */
struct ship
{
    char name[SHIP_NAME_LEN];
    int pos_x, pos_y;
    int len;
    int dir;
};

struct player_controls
{
    void (*place_ships)(int player_idx);
    void (*take_turn)(int player_idx);
};

struct ai_state
{
    int state;
    int row, col;
    int dir;

    /* 
     * if there are two adjacent ships oriented in the same direction,
     * the AI can be fooled into thinking there's one ship which is oriented
     * in the perpindicular direction.  If this happens, it needs to revert row
     * and col to row_start and col_start.
     */
    int row_start, col_start;

    /*
     * If the scenario described above happens, the AI will track the
     * places where it has hit other ships in pending_rows and pending_cols
     */
    int n_pending;
    int *pending_rows, *pending_cols;
};

/*
 * each row is represented as a three-bit field.
 * Only the first 30 bits of each row are used.
 * This will not work on platforms where sizeof(int) < 4
 */
extern struct player
{
    /* Player's name to be displayed */
    char name[PLAYER_NAME_LEN];

    struct player_controls const *controls;

    /* positions of the player's battleships */
    int ships[10];
    /* places where the player has fired */
    int shots[10];

    /* meta-data used for announcements; see comment above struct ship def */
    int ship_count;
    struct ship ship_meta[N_SHIPS];

    /* this is only used by the AI */
    struct ai_state ais;
} players[1 + PLAYER_2];

extern struct player_controls ai_easy_controls;
extern struct player_controls human_controls;

int ship_put(struct player *player, char const *ship_name, int row, int col,
             int len, int dir);

/* fire a shot at the other player.  return 1 if there is a hit; else return 0.
 * the player_idx parameter refers to the player who is shooting, not the player
 * who is getting shot at.
 */
int fire(int player_idx, int row, int col);

void player_place_ships(int player_idx);
void player_take_turn(int player_idx);
void player_set_name(int player_idx, char const *name);
char const *player_get_name(int player_idx);
int grid_get(int const *grid, int row, int col);

/*
 * returns strings representing col, row as they should be printed
 */
char const *col_name(int col);
char const *row_name(int row);
